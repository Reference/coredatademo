//
//  Song.m
//  CoreDataDemo
//
//  Created by scott.8an@gmail.com on 12-4-3.
//  Copyright (c) 2012年 littleworl,inc. All rights reserved.
//

#import "Song.h"


@implementation Song

@dynamic frontCoverPath;
@dynamic name;
@dynamic sect;
@dynamic language;
@dynamic size;
@dynamic artist;
@dynamic type;
@dynamic songId;

- (NSString*)nameWithSect:(Sect)sect{
    switch (sect) {
        case Rock:
            return @"Rock";
            break;
        case Jazz:
            return @"Jazz";
            break;
        case Pop:
            return @"Pop";
            break;
        case Rap:
            return @"Rap";
            break;
        case NewCentury:
            return @"NewCentury";
            break;
        default:
            return nil;
            break;
    }
}

- (NSString*)nameWithMusicType:(MusicType)type{
    switch (type) {
        case Mp3:
            return @"Mp3";
            break;
        case WAV:
            return @"Wav";
            break;
        case FLC:
            return @"Flac";
            break;
        case M4V:
            return @"M4v";
            break;
        default:
            return nil;
            break;
    }
}
@end
