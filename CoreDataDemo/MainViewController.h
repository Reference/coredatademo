//
//  MainViewController.h
//  CoreDataDemo
//
//  Created by scott.8an@gmail.com on 12-4-3.
//  Copyright (c) 2012年 littleworl,inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AppDelegate;

@interface MainViewController : UIViewController<UITableViewDelegate,
UITableViewDataSource,NSFetchedResultsControllerDelegate>{
    UITableView    *mainTableView;
    
    AppDelegate    *shareAppDelegate;
    NSFetchedResultsController *fetchedResultsController_;
}

@property (nonatomic,retain) NSFetchedResultsController *fetchedResultsController;

@end
