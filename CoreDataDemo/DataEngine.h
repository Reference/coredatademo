//
//  DataEngine.h
//  CoreDataDemo
//
//  Created by scott.8an@gmail.com on 12-4-3.
//  Copyright (c) 2012年 littleworl,inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataType.h"
#import "Song.h"

@class AppDelegate;

@interface DataEngine : NSObject{
    NSManagedObjectContext  *managedContext_;
    AppDelegate             *shareAppDelegate;
}
@property (nonatomic,readonly) NSManagedObjectContext  *managedContext;

+ (DataEngine*)shareDataEngine;
- (void)saveContext;
- (NSManagedObject*)managedObjectForEntityName:(NSString*)name inContext:(NSManagedObjectContext*)context;

//Song related
- (Song*)getSongWithId:(NSString*)sId inContext:(NSManagedObjectContext*)context;
- (void)removeSongById:(NSString*)sId inContext:(NSManagedObjectContext*)context;
- (void)clearUpAllSongsInContext:(NSManagedObjectContext*)context;
- (void)updateSong:(Song*)song withDictionary:(NSDictionary*)dict;

@end
