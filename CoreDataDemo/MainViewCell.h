//
//  MainViewCell.h
//  CoreDataDemo
//
//  Created by scott.8an@gmail.com on 12-4-3.
//  Copyright (c) 2012年 littleworl,inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewCell : UITableViewCell

+ (CGFloat)heightWithName:(NSString*)name 
                feedImage:(UIImage*)img 
                  comment:(NSString*)comment;
@end
