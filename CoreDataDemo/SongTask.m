//
//  SongTask.m
//  CoreDataDemo
//
//  Created by scott.8an@gmail.com on 12-4-3.
//  Copyright (c) 2012年 littleworl,inc. All rights reserved.
//

#import "SongTask.h"
#import "DataEngine.h"
#import "Song.h"
#import "DataType.h"

NSString *const SongGetSuccessNotification = @"SongGetSuccessNotification";

@implementation SongTask

- (id)initSongTask{
    if (self = [super init]) {
        NSArray *arr = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Songs" ofType:@"plist"]];
        if (arr && [arr count]) {
            for (NSDictionary *dic in arr) {
                //先清除数据库中的数据,避免重复
                NSString *songId = [dic objectForKey:@"songId"];
                [[DataEngine shareDataEngine] removeSongById:songId inContext:[DataEngine shareDataEngine].managedContext];
                
                //创建一条数据
                Song *song = (Song*)[[DataEngine shareDataEngine] managedObjectForEntityName:SongEntityName inContext:[DataEngine shareDataEngine].managedContext];
            
                //更新数据内容
                [[DataEngine shareDataEngine] updateSong:song withDictionary:dic];
                
                
            }
            //提交
            [[DataEngine shareDataEngine] saveContext];
            
            //发送通知，操作完成
            [[NSNotificationCenter defaultCenter] postNotificationName:SongGetSuccessNotification
                                                                object:nil
                                                              userInfo:nil];
        }
    }
    return self;
}
@end
