//
//  MainViewController.m
//  CoreDataDemo
//
//  Created by scott.8an@gmail.com on 12-4-3.
//  Copyright (c) 2012年 littleworl,inc. All rights reserved.
//

#import "MainViewController.h"
#import "AppDelegate.h"
#import "MainViewCell.h"
#import "DataEngine.h"
#import "DataType.h"
#import "SongTask.h"

@interface MainViewController ()

@end

@implementation MainViewController
@synthesize fetchedResultsController = fetchedResultsController_;

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SongGetSuccessNotification object:nil];
    
    fetchedResultsController_.delegate = nil;
    [fetchedResultsController_ release];
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"歌曲信息";
    mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460-self.navigationController.navigationBar.frame.size.height)
                                                 style:UITableViewStylePlain];
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
   // mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:mainTableView];
    [mainTableView release];

    shareAppDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleSongGetSuccessNotification:)
                                                 name:SongGetSuccessNotification
                                               object:nil];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    SongTask *task = [[SongTask alloc] initSongTask];
    [task release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark- handle notifications
- (void)handleSongGetSuccessNotification:(NSNotification*)notif{
    [mainTableView reloadData];
}

#pragma mark- UITableViewDelegate

#pragma mark- UITableViewDataSource
- (void)configMainCell:(MainViewCell*)cell atIndex:(NSIndexPath*)indexPath{
    Song *song = (Song*)[self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:@"defAlbum.png"];
    cell.textLabel.text = song.name;
    cell.detailTextLabel.text = song.artist;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.fetchedResultsController.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *tableViewCellIdentifier = @"tableViewCellIdentifier";
    
    MainViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableViewCellIdentifier];
    if (!cell) {
        cell = [[[MainViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:tableViewCellIdentifier] autorelease];
    }
    
    [self configMainCell:cell atIndex:indexPath];
    return cell;
}

#pragma mark- NSFetchedResultsController
- (NSFetchedResultsController*)fetchedResultsController{
    if (fetchedResultsController_) {
        return fetchedResultsController_;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:SongEntityName inManagedObjectContext:[DataEngine shareDataEngine].managedContext];
    [fetchRequest setEntity:entity];

    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"songId" ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:[DataEngine shareDataEngine].managedContext
                                                                                                  sectionNameKeyPath:nil
                                                                                                           cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    //remove from memory
    [fetchRequest release];
    [aFetchedResultsController release];
    
    
    NSError *error = nil;
    if (![fetchedResultsController_ performFetch:&error]) {
        abort();
    }
    return fetchedResultsController_;
}

#pragma mark- 
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller{
    mainTableView.userInteractionEnabled = NO;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    mainTableView.userInteractionEnabled = YES;
    [mainTableView reloadData];
}
@end
