//
//  Song.h
//  CoreDataDemo
//
//  Created by scott.8an@gmail.com on 12-4-3.
//  Copyright (c) 2012年 littleworl,inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef enum {
    Rock = 1,
    Jazz,
    Pop,
    Rap,
    NewCentury
}Sect;

typedef enum {
    Mp3 = 1,
    WAV,
    FLC,
    M4V
}MusicType;

@interface Song : NSManagedObject

@property (nonatomic, retain) NSString * frontCoverPath;//封面图片地址
@property (nonatomic, retain) NSString * name;//歌曲名
@property (nonatomic, retain) NSNumber * sect;//流派
@property (nonatomic, retain) NSString * language;
@property (nonatomic, retain) NSNumber * size;//bytes
@property (nonatomic, retain) NSString * artist;//艺术家名
@property (nonatomic, retain) NSNumber * type;//音乐类型
@property (nonatomic, retain) NSNumber * songId;

- (NSString*)nameWithSect:(Sect)sect;

- (NSString*)nameWithMusicType:(MusicType)type;
@end
