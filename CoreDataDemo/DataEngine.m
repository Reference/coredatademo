//
//  DataEngine.m
//  CoreDataDemo
//
//  Created by scott.8an@gmail.com on 12-4-3.
//  Copyright (c) 2012年 littleworl,inc. All rights reserved.
//

#import "DataEngine.h"
#import "AppDelegate.h"

NSString *const SongEntityName = @"Song";

static DataEngine *dataEngine_ = nil;

@implementation DataEngine
@synthesize managedContext = managedContext_;

- (id)init{
    if (self = [super init]) {
        shareAppDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    }
    return self;
}

+ (DataEngine*)shareDataEngine{
    if (!dataEngine_) {
        dataEngine_ = [[self alloc] init];
    }
    return dataEngine_;
}

- (NSManagedObjectContext*)managedContext{
    if (managedContext_) {
        return managedContext_;
    }
    NSPersistentStoreCoordinator *coordinator = [shareAppDelegate persistentStoreCoordinator];
    if (coordinator != nil) {
        managedContext_ = [[NSManagedObjectContext alloc] init];
        [managedContext_ setPersistentStoreCoordinator:coordinator];
    }
    return managedContext_;
}

- (void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

- (NSManagedObject*)managedObjectForEntityName:(NSString*)name inContext:(NSManagedObjectContext*)context{
    return [NSEntityDescription insertNewObjectForEntityForName:name inManagedObjectContext:context];
}

//Song related
- (Song*)getSongWithId:(NSString*)sId inContext:(NSManagedObjectContext*)context{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:SongEntityName inManagedObjectContext:context];
    [fetchRequest setEntity:entity];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"songId == %@", [NSNumber numberWithInteger:[sId integerValue]]];
    [fetchRequest setPredicate:predicate];

    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];

    if (fetchedObjects && [fetchedObjects count]) {
        return [fetchedObjects objectAtIndex:0];
    }
    return nil;
}

- (void)removeSongById:(NSString*)sId inContext:(NSManagedObjectContext*)context{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:SongEntityName inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"songId == %@", [NSNumber numberWithInteger:[sId integerValue]]];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];
    
    if (fetchedObjects && [fetchedObjects count]) {
        for (NSManagedObject *obj in fetchedObjects) {
            [context deleteObject:obj];
        }
        [context save:&error];
    }
}

- (void)clearUpAllSongsInContext:(NSManagedObjectContext*)context{
    NSFetchRequest *fRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:SongEntityName inManagedObjectContext:context];
    [fRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *fetchedResults = [context executeFetchRequest:fRequest error:&error];
    [fRequest release];
    
    if (fetchedResults && [fetchedResults count]) {
        for (NSManagedObject *obj in fetchedResults) {
            [context deleteObject:obj];
        }
        [context save:&error];
    }
}

- (void)updateSong:(Song*)song withDictionary:(NSDictionary*)dict{
    if (dict && [[dict objectForKey:@"songId"] intValue]>0) {
        song.songId = [dict objectForKey:@"songId"];
    }
    if (dict && [[dict objectForKey:@"type"] intValue]>0) {
        song.type = [dict objectForKey:@"type"];
    }
    if (dict && [[dict objectForKey:@"sect"] intValue]>0) {
        song.sect = [dict objectForKey:@"sect"];
    }
    if (dict && [[dict objectForKey:@"size"] intValue]>0) {
        song.size = [dict objectForKey:@"size"];
    }
    if (dict && [dict objectForKey:@"name"] && [[dict objectForKey:@"name"] length]) {
        song.name = [dict objectForKey:@"name"];
    }
    if (dict && [dict objectForKey:@"language"] && [[dict objectForKey:@"language"] length]) {
        song.language = [dict objectForKey:@"language"];
    }
    if (dict && [dict objectForKey:@"artist"] && [[dict objectForKey:@"artist"] length]) {
        song.artist = [dict objectForKey:@"artist"];
    }
    if (dict && [dict objectForKey:@"frontCoverPath"] && [[dict objectForKey:@"frontCoverPath"] length]) {
        song.frontCoverPath = [dict objectForKey:@"frontCoverPath"];
    }   
}
@end
